import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Espace } from '../models/espace';

@Injectable({
  providedIn: 'root'
})
export class EspaceService {
  private baseUrl = 'http://localhost:8087/espace';
  constructor(private http:HttpClient) { }

   // tslint:disable-next-line:typedef
   getAll()
   {
     return this.http.get(this.baseUrl + '/all');
   }

   addespace(e:Espace):Observable<object>{
    return this.http.post("http://localhost:8087/espace" ,e ).pipe()

  }

  getEspaceById(id: number): Observable<any>
  {
    return this.http.get(this.baseUrl  + id);
  }

  supprimer(id: number): Observable<any>
  {
    return this.http.delete(this.baseUrl +  "/" +id, { responseType: 'text' });
  }
   update(esp:Espace): any
   {
     return this.http.put(this.baseUrl ,esp).pipe();
   }

}
