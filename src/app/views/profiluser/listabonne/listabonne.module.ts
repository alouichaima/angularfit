import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListabonneRoutingModule } from './listabonne-routing.module';
import { ListabonneComponent } from './listabonne.component';


@NgModule({
  declarations: [
    ListabonneComponent
  ],
  imports: [
    CommonModule,
    ListabonneRoutingModule
  ]
})
export class ListabonneModule { }
