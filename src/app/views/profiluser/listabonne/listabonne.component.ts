import { UserCRUDService } from 'src/app/service/user-crud.service';
import { utilisateur } from './../../../models/utilisateur.model';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Route } from '@angular/router';

@Component({
  selector: 'app-listabonne',
  templateUrl: './listabonne.component.html',
  styleUrls: ['./listabonne.component.css']
})
export class ListabonneComponent implements OnInit {

  utilisateur: Observable<utilisateur[]> | undefined;

  constructor(private UserCRUDService: UserCRUDService) { }

  ngOnInit(): void {
    this.reloadData();

  }

  reloadData(): void {
    // @ts-ignore
    this.utilisateur = this.UserCRUDService.getByRole();
  }


}
