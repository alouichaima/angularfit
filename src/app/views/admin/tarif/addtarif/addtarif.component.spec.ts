import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddtarifComponent } from './addtarif.component';

describe('AddtarifComponent', () => {
  let component: AddtarifComponent;
  let fixture: ComponentFixture<AddtarifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddtarifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddtarifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
