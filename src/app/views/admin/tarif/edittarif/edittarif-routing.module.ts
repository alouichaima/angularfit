import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EdittarifComponent } from './edittarif.component';

const routes: Routes = [
  {path:'',component:EdittarifComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EdittarifRoutingModule { }
