import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdittarifComponent } from './edittarif.component';

describe('EdittarifComponent', () => {
  let component: EdittarifComponent;
  let fixture: ComponentFixture<EdittarifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdittarifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdittarifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
