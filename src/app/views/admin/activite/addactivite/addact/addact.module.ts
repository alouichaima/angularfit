import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddactRoutingModule } from './addact-routing.module';
import { AddactComponent } from './addact.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AddactComponent
  ],
  imports: [
    CommonModule,
    AddactRoutingModule,
    FormsModule
  ]
})
export class AddactModule { }
