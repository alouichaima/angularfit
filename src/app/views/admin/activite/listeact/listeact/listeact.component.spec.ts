import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeactComponent } from './listeact.component';

describe('ListeactComponent', () => {
  let component: ListeactComponent;
  let fixture: ComponentFixture<ListeactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListeactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
