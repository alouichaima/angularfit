import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TarifService } from 'src/app/service/tarif.service';

@Component({
  selector: 'app-tarif',
  templateUrl: './tarif.component.html',
  styleUrls: ['./tarif.component.css']
})
export class TarifComponent implements OnInit {
  listetarif: any;

  constructor(private servicetarif : TarifService, private router : Router) { }

  ngOnInit(): void {
    this.getall();
  }

  getall():void{

    this.servicetarif.getAll().subscribe({next: (data) => {

    this.listetarif= data;

    console.log(data);

    },

    error: (t) => console.error(t)

    });

      }

}
